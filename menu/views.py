from django.shortcuts import render
from .models import Menu


def index(request):
    return render(request, 'index.html', {'all_menu': Menu.objects.all()})


def menu(request, menu_slug, item_slug=None):
    return render(request, 'menu.html', {'menu_slug': menu_slug, 'item_slug': item_slug})
