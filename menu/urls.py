from django.urls import path
from .views import index, menu

urlpatterns = [
    path('', index, name='index_url'),
    path('<str:menu_slug>/', menu, name='menu_url'),
    path('<str:menu_slug>/<str:item_slug>/', menu, name='item_url')
]