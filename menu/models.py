from django.db import models

from django.urls import reverse


class Menu(models.Model):
    name = models.CharField(max_length=50, verbose_name='Menu title')
    slug = models.SlugField(max_length=50, verbose_name='Menu slug')

    def get_url(self):
        return reverse('menu_url', kwargs={'menu_slug': self.slug})

    def __str__(self):
        return self.name


class MenuItem(models.Model):
    menu = models.ForeignKey(Menu, null=True, blank=True, on_delete=models.CASCADE, related_name='items',
                             verbose_name='Menu')
    parent = models.ForeignKey('self', null=True, blank=True, on_delete=models.CASCADE, related_name='items',
                               verbose_name='Parent')

    name = models.CharField(max_length=50, verbose_name='Item name')
    slug = models.SlugField(max_length=50, verbose_name='Item slug')

    def get_item_generation(self):
        generation = 1
        item = self
        while True:
            if not item.parent:
                return generation
            else:
                generation += 1
                item = item.parent

    def get_url(self):
        current_item = self
        while True:
            if not current_item.menu:
                current_item = current_item.parent
            else:
                return reverse('item_url', kwargs={'menu_slug': current_item.menu.slug, 'item_slug': self.slug})

    def __str__(self):
        return self.name
