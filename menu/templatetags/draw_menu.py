from django import template
from menu.models import Menu

register = template.Library()

@register.inclusion_tag('template_tags/draw_menu.html', takes_context=True)
def draw_menu(context, menu_slug, item_slug=None):
    try:
        menu = Menu.objects.prefetch_related('items__items__items__items').get(slug=menu_slug)
        if item_slug:
            tree = get_tree(menu, item_slug)
        else:
            tree = [{item.get_item_generation(): item} for item in menu.items.all()]
            tree.insert(0, {0: menu})
        return {'menu': menu, 'context': context, 'tree': tree}
    except Menu.DoesNotExist:
        return {'menu': '', 'context': context}


def get_current_item(items, slug, current_item=None):
    for item in items.all():
        if item.slug == slug:
            return item
        else:
            if item.items:
                current_item = get_current_item(item.items, slug, current_item)

        if current_item:
            return current_item


def get_parents(item, parents):
    parents.append(item)

    if item.parent:
        get_parents(item.parent, parents)
    return parents


def get_tree(menu, slug):
    tree = [{0: menu}]
    if not menu.items:
        return tree
    item = get_current_item(menu.items, slug)
    parents = get_parents(item, [])

    current_item = menu
    for i in range(len(parents)):
        j = 0
        while True:
            item_to_append = current_item.items.all()[j]
            tree.append({item_to_append.get_item_generation(): item_to_append})
            if item_to_append == parents[len(parents) - 1 - i]:
                break
            j += 1
        current_item = item_to_append
    if item.items:
        for child in item.items.all():
            tree.append({child.get_item_generation(): child})

    for i in range(len(parents)):
        if parents[i].parent:
            current_item = parents[i].parent
        else:
            current_item = parents[i].menu

        flag = False
        for child in current_item.items.all():
            if flag:
                tree.append({child.get_item_generation(): child})

            if child == parents[i]:
                flag = True

    return tree
