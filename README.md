#  Tree menu #

### Based on [hobbygames.ru](https://hobbygames.ru/) categories

### Set up and run:

1. Clone project and go to it

`git clone git@gitlab.com:tovarischduraley/tree-menu.git`

`cd tree-menu`

2. Make and activate venv

`python -m venv venv`

`venv\Scripts\activate`

3. Install Django and apply migrations

`pip install Django`

`python manage.py migrate`

4. Load menu data

`python manage.py loaddata menu_dump.json`

5. Run app

`python manage.py runserver`

